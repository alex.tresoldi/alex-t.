const controllerSobre = require('../../Controllers/sobre.controller')

module.exports = routeV1 => {
  routeV1.route('/sobre').get(controllerSobre.getSobre)
}
