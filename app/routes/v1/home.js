const homeController = require('../../Controllers/home.controller')

module.exports = routeV1 => {
  routeV1.route('/').get(homeController.getHome)
}
