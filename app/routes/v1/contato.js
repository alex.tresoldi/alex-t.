const contatoController = require('../../Controllers/contato.controller')

const middlweareValidate = require('../../utils/dtoValidate')

module.exports = routeV1 => {
  routeV1
    .route('/contato')
    .get(contatoController.getContato)
    .post(
      middlweareValidate('body', contatoController.postContatoSchema),
      contatoController.postContato
    )
}
