const express = require('express')
const v1Home = require('./v1/home')
const v1Sobre = require('./v1/sobre')
const v1Contato = require('./v1/contato')
module.exports = app => {
  const routev1 = express.Router()
  v1Home(routev1)
  v1Sobre(routev1)
  v1Contato(routev1)
  app.use('', routev1)
}
