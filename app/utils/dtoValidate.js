module.exports = (contexto, schema) => {
  return async (req, res, next) => {
    // Aqui é o codigo do middleware
    console.log(req[contexto])

    /* const resultado = contatoController.postContatoSchema.validate(req.body, {
      abortEarly: false
    })
 */
    const resultado = schema.validate(req[contexto], {
      abortEarly: false
    })
    if (resultado.error) {
      console.log('errors')
      /* return res.status(400).send(resultado.error) */
      return res.render('errors', { errors: resultado.error })
    }

    next()
  }
}
