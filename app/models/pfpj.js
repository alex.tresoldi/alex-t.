const pfpj = [
  {
    id: 1,
    descricao: 'Pessoa Física'
  },
  {
    id: 2,
    descricao: 'Pessoa Jurídica'
  }
]

const listaTodos = () => {
  return pfpj
}

const BuscaPorID = id => {
  const result = pfpj.filter(item => {
    return item.id === parseInt(id, 10)
  })

  return result.length > 0 ? result[0] : undefined
}

const novopfpj = ({ id, descricao }) => {
  pfpj.push({ id, descricao })
}

module.exports = {
  listaTodos,
  BuscaPorID,
  novopfpj
}
