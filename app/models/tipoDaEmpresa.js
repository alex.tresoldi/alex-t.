const empresa = [
  {
    id: 1,
    nome: 'Microempresa (ME)'
  },
  {
    id: 2,
    nome: 'Empresa de Pequeno Porte (EPP)'
  },
  {
    id: 3,
    nome: 'Empresa de médio porte'
  },
  {
    id: 4,
    nome: 'Grandes empresas'
  }
]

const listaTodos = () => {
  return empresa
}

const BuscaPorID = id => {
  const result = empresa.filter(item => {
    return item.id === parseInt(id, 10)
  })

  return result.length > 0 ? result[0] : undefined
}

const novopfpj = ({ id, descricao }) => {
  empresa.push({ id, descricao })
}

module.exports = {
  listaTodos,
  BuscaPorID,
  novopfpj
}
