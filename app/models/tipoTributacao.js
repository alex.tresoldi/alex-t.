const tributacao = [
  {
    id: 1,
    descricao: 'Imposto de Renda'
  },
  {
    id: 2,
    descricao: 'Lucro Real'
  },
  {
    id: 3,
    descricao: 'Simples Nacional'
  },
  {
    id: 4,
    descricao: 'Lucro Presumido'
  }
]

const listaTodos = () => {
  return tributacao
}

const BuscaPorID = id => {
  const result = tributacao.filter(item => {
    return item.id === parseInt(id, 10)
  })

  return result.length > 0 ? result[0] : undefined
}

const novopfpj = ({ id, descricao }) => {
  tributacao.push({ id, descricao })
}

module.exports = {
  listaTodos,
  BuscaPorID,
  novopfpj
}
