const pfpj = require('../models/pfpj')
const tributacao = require('../models/tipoTributacao')
const empresa = require('../models/tipoDaEmpresa')
const optionsMapper = require('../utils/mappers/selectOptionsMapper')

const fs = require('fs')
const Joi = require('joi')
const ejs = require('ejs')
const htmltopdf = require('html-pdf-node')
const path = require('path')
const { join } = require('path')

const getContato = (req, res, next) => {
  // construir viewmodel
  const viewModel = {
    opcoespfpj: optionsMapper('descricao', 'id', pfpj.listaTodos()),

    opcoesTributacao: optionsMapper('descricao', 'id', tributacao.listaTodos()),

    empresa: optionsMapper('nome', 'id', empresa.listaTodos())
  }
  res.render('contato', viewModel)
}
// montar viewmodel
const postContato = (req, res, next) => {
  const {
    nomeEmpresa,
    opcoespfpj,
    email,
    numero,
    tipodeEmpresa,
    opcoesTributacao,
    duvidatxt
  } = req.body

  const pfselecionado = pfpj.BuscaPorID(opcoespfpj)

  const tributacaoselecionado = tributacao.BuscaPorID(opcoesTributacao)

  const empresaselecionado = empresa.BuscaPorID(tipodeEmpresa)

  const pdfViewModel = {
    nomeEmpresa,
    opcoespfpj: pfselecionado.descricao,
    email,
    numero,
    tipodeEmpresa: empresaselecionado.nome,
    opcoesTributacao: tributacaoselecionado.descricao,
    duvidatxt
  }

  // montar html

  const filePath = path.join(__dirname, '../views/contatoPDF.ejs')
  console.log(filePath)
  const templateHtml = fs.readFileSync(filePath, 'utf8')

  /* const filePath = path.join(__dirname, 'views/contatoPDF.ejs')

  const templateHtml = fs.readFileSync('views/contatoPDF.ejs', 'utf8') */

  // montar pdf
  const htmlPronto = ejs.render(templateHtml, pdfViewModel)

  // retornar pdf
  const file = {
    content: htmlPronto
  }

  const configuracoes = {
    format: 'A4',
    printBackground: true
  }

  htmltopdf.generatePdf(file, configuracoes).then(resultPromessa => {
    res.contentType('application/pdf')
    res.send(resultPromessa)
  })
}

const postContatoSchema = Joi.object({
  nomeEmpresa: Joi.string().max(30).min(5).required(),
  opcoespfpj: Joi.string().required(),
  email: Joi.string().email().required(),
  numero: Joi.number().required(),
  tipodeEmpresa: Joi.string().required(),
  opcoesTributacao: Joi.string().required(),
  duvidatxt: Joi.string().required()
})

module.exports = {
  getContato,
  postContato,
  postContatoSchema
}
