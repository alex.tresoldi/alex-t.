// Descrição do servidor do express //
const express = require('express')
const servidor = express()
const path = require('path')
const bp = require('body-parser')

const rotas = require('./routes/rotas')

servidor.use(bp.json())
servidor.use(bp.urlencoded())

servidor.use(express.static(path.join(__dirname, 'public')))

servidor.set('views', path.join(__dirname, 'views'))
servidor.set('view engine', 'ejs')

rotas(servidor)

const porta = 5000

servidor.listen(porta, () => {
  console.log('O Servidor está rodando na porta ', porta)
})
